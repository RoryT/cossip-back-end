﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoSSIP___Back_End
{
    class SQLStatements
    {

        private const string PATHWAYS_JOINING_CUSTOMER_REQUEST_ALL_DATA = "select request.requnum as PWRequestNumber, request.daterecv as dateRecieved, "
            + "request.daterespb as dateRespondBy, request.dateresp as dateResponded, request.priority as priority, request.timetakenn as timeTaken, request.timetakenu as timetakenUnit, "
            + "requestType.descr as requestType, requestType.reqtcode as requestCode, recievingOfficer.username as PWRecievingOfficer, actioningOfficer.username as PWActioningOfficer, "
            + "requestee.descr as requesteeType, reqstatus.descr as requestStatus, contactMethod.descr as contactMethod, request.tpkacrrequ as PWReqKey "
            + "from pthdbo.acrrequ as request "
            + "JOIN pthdbo.acrcont as contactMethod on request.tfkacrcont = contactMethod.tpkacrcont "
            + "JOIN pthdbo.acrreqt as requestType on request.tfkacrreqt = requestType.tpkacrreqt "
            + "JOIN pthdbo.csyuser as actioningOfficer on request.tfkcsyuser = actioningOfficer.tpkcsyuser "
            + "JOIN pthdbo.csyuser as recievingOfficer on request.tfkuser_1 = recievingOfficer.tpkcsyuser "
            + "JOIN pthdbo.csyuser as respondingOfficer on request.tfkuser_3 = respondingOfficer.tpkcsyuser "
            + "JOIN pthdbo.acrrtor as requestee on request.tfkacrrtor = requestee.tpkacrrtor "
            + "JOIN pthdbo.acrstat as reqstatus on request.tfkacrstat = reqstatus.tpkacrstat ";

        private const string PATHWAYS_NOTES_CUSTOMER_REQUEST_ALL_DATA = "select NOTES.tpkacrnote as PWNoteKey, NOTES.tfkacrrequ as PWReqKey, Author.username as author, NOTES.datetime as date, notetype.descr as noteType, NOTES.note as note "
            + "from pthdbo.acrnote as NOTES JOIN pthdbo.csyuser as Author on NOTES.tfkcsyuser = Author.tpkcsyuser JOIN pthdbo.acrntyp as notetype on NOTES.tfkacrntyp = notetype.tpkacrntyp";

        private const string CONFIRM_JOINING_ENQUIRY_REQUEST_ALL_DATA = "select enquiry.enquiry_number as CRequestNumber, enquiry.ext_system_ref as PWRequestNumber, enquiry.enquiry_desc as requestDescription, "
            + "serviceType.service_code as serviceCode, serviceType.service_name as serviceType, enquiry.enquiry_name as requesteeName, "
            + "enquiry.enquiry_address as requestAddress, enquiry.site_code as siteCode, enquiry.enquiry_phone as requesteeHomephone, "
            + "enquiry.enquiry_phone_2 as requesteeMobilephone, enquiry.enquiry_fax as requesteeFax, enquiry.email_address as requesteeEmail, "
            + "officer.officer_code as actionOfficerCode, officer.officer_name as actionOfficer, enquiryLinks.plot_number as plotNumber, job.job_number as lastJob, "
            + "job.job_notes as lastJobNotes "
            + "from dbo.central_enquiry as enquiry "
            + "JOIN grounds.enquiry as enquiryLinks on enquiryLinks.enquiry_number = enquiry.enquiry_number "
            + "JOIN grounds.job as job on job.job_number = enquiryLinks.job_number "
            + "JOIN dbo.action_officer as officer on enquiry.officer_code = officer.officer_code "
            + "JOIN dbo.type_of_service as serviceType on enquiry.service_code = serviceType.service_code ";

        private const string CONFIRM_JOBS_ALL_DATA = "select job.job_number as jobNumber, job.site_code as siteCode, job.plot_number as plotNumber, job.job_log_number as jobLogNumber, job.job_entry_date as jobEnteredDate, "
            + "job.job_notes as jobNotes, job.job_location as jobLocation, job.cost_code as costCode, customer.customer_name as customerName, job.contract_code as contractCode, "
            + "job.job_complete_date as jobCompletedDate, job.contract_area_code as contractAreaCode "
            + "from grounds.job as job "
            + "JOIN grounds.customer as customer on customer.customer_code = job.customer_code ";

        private const string EMPTY_TABLE = "TRUNCATE TABLE ";

        public static string GetPathwaysCustomerRequestData()
        {

            return GetPathwaysCustomerRequestData("");
        }

        public static string GetConfirmCustomerRequestData()
        {

            return GetConfirmCustomerRequestData("");
        }

        public static string GetConfirmCustomerRequestData(string condition)
        {

            return CONFIRM_JOINING_ENQUIRY_REQUEST_ALL_DATA + addSpace(condition);
        }

        public static string GetPathwaysCustomerRequestData(string condition)
        {

            return PATHWAYS_JOINING_CUSTOMER_REQUEST_ALL_DATA + addSpace(condition);
        }

        public static string GetPathwaysCustomerRequestDataByRequestNumber(int requestNumber)
        {
            return GetPathwaysCustomerRequestData("WHERE request.requnum = " + requestNumber);
        }

        public static string getPathwaysCustomerRequestDataByRequestPrimaryKey(int requestPrimaryKey)
        {
            return GetPathwaysCustomerRequestData("WHERE request.tpkacrrequ = " + requestPrimaryKey);
        }

        public static string GetPathwaysCustomerRequestNotes(string condition)
        {
            return PATHWAYS_NOTES_CUSTOMER_REQUEST_ALL_DATA + addSpace(condition);
        }

        public static string GetPathwaysCustomerRequestNotes()
        {
            return GetPathwaysCustomerRequestNotes("");
        }

        public static string getConfirmJobsByJobNumber(int jobNumber)
        {
            return getConfirmJobs("WHERE job.job_number = " + jobNumber);
        }

        private static string getConfirmJobs(string condition)
        {
            return CONFIRM_JOBS_ALL_DATA + addSpace(condition);
        }

        public static string getConfirmJobs()
        {
            return getConfirmJobs("");
        }

        public static string EmptyTable(string table)
        {
            return EMPTY_TABLE + table;
        }

        private static string addSpace(string str)
        {
            StringBuilder sb = new StringBuilder(str);

            if (str == null || str == "" || str == " ")
            {
                return " ";
            }

            if (str[0] == ' ')
            {
                return str;
            }

            sb.Insert(0, ' ');

            return sb.ToString();
        }

       
    }
}
