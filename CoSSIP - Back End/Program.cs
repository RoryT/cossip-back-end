﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoSSIP___Back_End
{
    class Program
    {

        const string refreshTime = "04:00:00";

        static void Main(string[] args)
        {

            while (true)
            {
                DatabaseConnectionManager.runUpdate();

                System.GC.Collect();
                System.GC.WaitForPendingFinalizers();

                string[] timeParts = refreshTime.Split(new char[1] { ':' });

                DateTime dateNow = DateTime.Now;
                DateTime date = new DateTime(dateNow.Year, dateNow.Month, dateNow.Day,
                           int.Parse(timeParts[0]), int.Parse(timeParts[1]), int.Parse(timeParts[2]));
                TimeSpan waitTime;

                if (date > dateNow)
                {
                    waitTime = date - dateNow;
                }
                else
                {
                    date = date.AddDays(1);
                    waitTime = date - dateNow;
                }

                //waits certan time and run the code
                Task.Delay(waitTime).Wait();
            }

        }
    }
}
