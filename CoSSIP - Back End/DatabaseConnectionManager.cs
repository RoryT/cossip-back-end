﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoSSIP___Back_End
{
    static class DatabaseConnectionManager
    {
        // DONT SET TO TRUE, TAKES DAYS UNTILL FIXED
        private const Boolean stitch = false;

        public enum Databases {
            confirm,
            pathways,
            sip
        }

        const string CONFIRM_DATABASE_CONNECTION_STRING = "Data Source=JS-DB02-T; Initial Catalog=entdev; Integrated Security=True";
        const string PATHWAYS_DATABASE_CONNECTION_STRING = "Data Source=JS-AS17-T; Initial Catalog=pthdev; Integrated Security=True";
        const string SIP_DATABASE_CONNECTION_STRING = "Data Source=JS-AS17-T; Initial Catalog=SIP_DB; Integrated Security=True;";


        public static Boolean runUpdate()
        {
            using (SqlConnection SQLConnection = new SqlConnection(CONFIRM_DATABASE_CONNECTION_STRING))
            {
                try
                {
                    SQLConnection.Open();
                    System.Diagnostics.Debug.WriteLine("Confirm DB Connection Success");

                    DataTable testData = RunQuery("", Databases.confirm);

                    SQLConnection.Close();
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine("Confirm DB Connection Failure");
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                }
            }

            using (SqlConnection SQLConnection = new SqlConnection(PATHWAYS_DATABASE_CONNECTION_STRING))
            {
                try
                {
                    SQLConnection.Open();
                    System.Diagnostics.Debug.WriteLine("Pathways DB Connection Success");

                    DataTable testData = RunQuery("", Databases.pathways);

                    SQLConnection.Close();
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine("Pathways DB Connection Failure");
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                }
            }

            try
            {

                DataTable pathwaysData = RunQuery(SQLStatements.GetPathwaysCustomerRequestData(), Databases.pathways);
                DataTable confirmData = RunQuery(SQLStatements.GetConfirmCustomerRequestData(), Databases.confirm);
                DataTable stitchedData = null;

                if (stitch)
                {
                    stitchedData = Stitch(pathwaysData, confirmData);

                    RunQuery(SQLStatements.EmptyTable("PW_Customer_Request"), Databases.sip);
                    BulkInsert(stitchedData, "PW_Customer_Request", Databases.sip);

                    stitchedData.Clear();
                    stitchedData.Dispose();
                    stitchedData = null;
                }
                else
                {
                    RunQuery(SQLStatements.EmptyTable("PW_Customer_Request"), Databases.sip);
                    BulkInsert(pathwaysData, "PW_Customer_Request", Databases.sip);

                    pathwaysData.Clear();
                    pathwaysData.Dispose();
                    pathwaysData = null;

                    RunQuery(SQLStatements.EmptyTable("C_Customer_Request"), Databases.sip);
                    BulkInsert(confirmData, "C_Customer_Request", Databases.sip);

                    confirmData.Clear();
                    confirmData.Dispose();
                    confirmData = null;
                }

                DataTable pathwaysNotes = RunQuery(SQLStatements.GetPathwaysCustomerRequestNotes(), Databases.pathways);

                RunQuery(SQLStatements.EmptyTable("PW_Notes"), Databases.sip);

                BulkInsert(pathwaysNotes, "PW_Notes", Databases.sip);

                pathwaysNotes.Clear();
                pathwaysNotes.Dispose();
                pathwaysNotes = null;

                DataTable confirmJobs = RunQuery(SQLStatements.getConfirmJobs(), Databases.confirm);

                RunQuery(SQLStatements.EmptyTable("C_Jobs"), Databases.sip);

                BulkInsert(confirmJobs, "C_Jobs", Databases.sip);

                confirmJobs.Clear();
                confirmJobs.Dispose();
                confirmJobs = null;


            }
            catch (Exception ex) {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }

            return true;
        }

        //DONT USE. TAKES DAYS / HOURS UNTILL FIXED
        private static DataTable Stitch(DataTable pathwaysData, DataTable confirmData)
        {
            System.Diagnostics.Debug.WriteLine("Started Stiching data together... This will take a while..");

            DateTime started = DateTime.Now;
            System.Diagnostics.Debug.WriteLine("Started At " + started);

            foreach (DataColumn column in confirmData.Columns)
            {
                if (!pathwaysData.Columns.Contains(column.ColumnName))
                {
                    pathwaysData.Columns.Add(column.ColumnName);
                }
            }

            int PWkeyIndex = pathwaysData.Columns.IndexOf("PWRequestNumber");
            int CkeyIndex = confirmData.Columns.IndexOf("PWRequestNumber");

            foreach (DataRow row in confirmData.Rows)
            {
                string value = row[CkeyIndex].ToString();

                if (value == null || value == "")
                {
                    continue;
                }

                string formated = string.Format("PWRequestNumber = {0}", value);

                DataRow[] rows = pathwaysData.Select(formated);

                if (rows.Length < 1)
                {
                    continue;
                }

                DataRow pathwayJoinRow = rows[0];

                Console.WriteLine("Stitching Request #" + value);

                foreach (DataColumn column in confirmData.Columns)
                {
                    pathwayJoinRow[column.ColumnName] = row[column];
                }
            }

            DateTime finished = DateTime.Now;
            System.Diagnostics.Debug.WriteLine("Stitching Finished At " + finished);
            System.Diagnostics.Debug.WriteLine("Took " + finished.Subtract(started));

            return pathwaysData;
        }

        public static void BulkInsert(DataTable data, string table, Databases db)
        {
            string connectionString;

            switch (db)
            {
                case Databases.confirm:
                    connectionString = CONFIRM_DATABASE_CONNECTION_STRING;
                    break;


                case Databases.pathways:
                    connectionString = PATHWAYS_DATABASE_CONNECTION_STRING;
                    break;

                case Databases.sip:
                default:
                    connectionString = SIP_DATABASE_CONNECTION_STRING;
                    break;
            }

            using (SqlConnection SQLConnection = new SqlConnection(connectionString))
            {

                using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(SQLConnection))
                {

                    System.Diagnostics.Debug.WriteLine("Running SQL Injection Command");

                    DateTime started = DateTime.Now;
                    System.Diagnostics.Debug.WriteLine("Started At " + started);

                    sqlBulkCopy.DestinationTableName = table;

                    foreach (var column in data.Columns)
                        sqlBulkCopy.ColumnMappings.Add(column.ToString(), column.ToString());

                    SQLConnection.Open();

                    sqlBulkCopy.BatchSize = 10000;
                    sqlBulkCopy.WriteToServer(data);

                    SQLConnection.Close();


                    DateTime finished = DateTime.Now;
                    System.Diagnostics.Debug.WriteLine("Finished At " + finished);
                    System.Diagnostics.Debug.WriteLine("Took " + finished.Subtract(started));
                }

            }
        }


        public static DataTable RunQuery(string command, Databases db)
        {

            System.Diagnostics.Debug.WriteLine("Running SQL Command: ");
            System.Diagnostics.Debug.WriteLine(command);

            DateTime started = DateTime.Now;
            System.Diagnostics.Debug.WriteLine("Started At " + started);

            string connectionString = "";
            DataTable dt = new DataTable();

            switch (db)
            {
                case Databases.confirm:
                    connectionString = CONFIRM_DATABASE_CONNECTION_STRING;
                    break;


                case Databases.pathways:
                    connectionString = PATHWAYS_DATABASE_CONNECTION_STRING;
                    break;

                case Databases.sip:
                    connectionString = SIP_DATABASE_CONNECTION_STRING;
                    break;
            }

            using (SqlConnection SQLConnection = new SqlConnection(connectionString))
            {
                SqlCommand SQLCommand = new SqlCommand(command, SQLConnection);

                try
                {
                    SQLConnection.Open();

                    // create data adapter
                    SqlDataAdapter dataAdapter = new SqlDataAdapter(SQLCommand);
                    // this will query your database and return the result to your datatable
                    dataAdapter.Fill(dt);

                    SQLConnection.Close();
                    dataAdapter.Dispose();
                }
                 catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine("Error connecting to Database: " + connectionString);
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                }
                finally
                {
                    SQLConnection.Close();

                    DateTime finished = DateTime.Now;
                    System.Diagnostics.Debug.WriteLine("Finished At " + finished);
                    System.Diagnostics.Debug.WriteLine("Took " + finished.Subtract(started));
                }
            }
            return dt;
        }

        public static void dataToCSV(DataTable data, string fileName, Boolean includeHeaders)
        {

            System.Diagnostics.Debug.WriteLine("Writing data to CSV: " + fileName);
            DateTime started = DateTime.Now;
            System.Diagnostics.Debug.WriteLine("Started At " + started);

            StreamWriter csvWriter = new StreamWriter(fileName + ".csv");

            if (includeHeaders)
            {
                IEnumerable<String> headers = data.Columns.OfType<DataColumn>().Select(column => sanitize(column.ColumnName));

                csvWriter.WriteLine(String.Join(",", headers));
            }

            IEnumerable<String> items = null;

            foreach (DataRow row in data.Rows)
            {
                items = row.ItemArray.Select(o => sanitize(o.ToString().Trim()));
                csvWriter.WriteLine(string.Join(",", items));
            }

            csvWriter.Flush();

            DateTime finished = DateTime.Now;
            System.Diagnostics.Debug.WriteLine("Finished At " + finished);
            System.Diagnostics.Debug.WriteLine("Took " + finished.Subtract(started));
        }

        private static string sanitize(string value)
        {
            return String.Concat("\"", value.Replace("\"", "\"\""), "\"");
        }
    }
}
